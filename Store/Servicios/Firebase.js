import * as firebase from 'firebase';

const config = {
  apiKey: "AIzaSyA-4bFwUdz4JYfu2Zd0WMedj1ApVO1Sh3M",
  authDomain: "reacti-native.firebaseapp.com",
  databaseURL: "https://reacti-native.firebaseio.com",
  projectId: "reacti-native",
  storageBucket: "",
  messagingSenderId: "660912578229"
};
firebase.initializeApp(config);

export const autenticacion = firebase.auth();
export const baseDeDatos = firebase.database();
